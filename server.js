const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const methodeOverride = require('method-override');
const expressLayouts = require('express-ejs-layouts');
const flash = require('express-flash');
const session = require('express-session');
const morgan = require('morgan');
const passport = require('passport');
const MongoStore = require('connect-mongo')(session);
const dotenv = require('dotenv');
const connectionMongo = require('./config/db');
const passportConfig = require('./config/passport-config');

// Load config
dotenv.config({ path: './config/config.env' });

// Passport config
passportConfig(passport);

// MongoDB connection
connectionMongo();

// Init server
const app = express();

// Morgan middleware
if (process.env.NODE_ENV === 'development') {
	app.use(morgan('dev'));
}

// Body parser middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(methodeOverride('_method'));

// Sessiona
app.use(
	session({
		secret: 'mysecretword',
		resave: false,
		saveUninitialized: false,
		store: new MongoStore({
			mongooseConnection: mongoose.connection,
		}),
	})
);

// Passport
app.use(passport.initialize());
app.use(passport.session());

// Flash messages
app.use(flash());

// Global vars
app.use((req, res, next) => {
	res.locals.success_msg = req.flash('success_msg');
	res.locals.error_msg = req.flash('error_msg');
	res.locals.error = req.flash('error');
	next();
});

// Uploads
// app.use('/uploads', express.static('uploads'));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
// app.use(express.static('uploads'));
app.use('/images', express.static(path.join(__dirname, 'images')));
// app.use(express.static('images'));

// Set Templating Engine
app.use(express.static('public'));
app.use(expressLayouts);
app.set('layout', './layouts/main');
app.set('view engine', 'ejs');

// Server routes
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/api/courses', require('./routes/courses'));
app.use('*', require('./routes/page404'));

// Ser server port
const PORT = process.env.PORT || 3000;
app.listen(PORT, () =>
	console.log(
		`Server running in ${process.env.NODE_ENV} mode on port ${PORT}...`
	)
);
