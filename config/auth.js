module.exports = {
	ensureAuth: function (req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		}
		req.flash(
			'error_msg',
			'Vous êtes pas authentifiez !!! Veuillez vous connectez'
		);
		res.redirect('/users/login');
	},
};
