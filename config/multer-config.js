const multer = require('multer');
const path = require('path');

const MIME_TYPES = {
	'application/pdf': 'pdf',
};

const storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, 'uploads');
	},
	filename: (req, file, callback) => {
		const name = file.originalname.split(' ').join('_');
		const removeExtFromName = name.split('.pdf')[0];
		const extension = MIME_TYPES[file.mimetype];
		callback(null, removeExtFromName + '_' + Date.now() + '.' + extension);
	},
});

module.exports = multer({
	storage: storage,
	fileFilter: function (req, file, callback) {
		var ext = path.extname(file.originalname);
		const { title, description, category } = req.body;
		if (!title || !description || !category || ext !== '.pdf') {
			return callback(null, false);
		}
		callback(null, true);
	},
}).single('fileUrl');
