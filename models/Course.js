const mongoose = require('mongoose');

const CourseSchema = mongoose.Schema({
	title: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: true,
	},
	category: {
		type: String,
		required: true,
	},
	fileUrl: {
		type: String,
		required: true,
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
	},
	create_at: {
		type: Date,
		default: Date.now,
	},
	comments: {
		type: Array,
		required: false,
	},
});

module.exports = mongoose.model('Course', CourseSchema);
