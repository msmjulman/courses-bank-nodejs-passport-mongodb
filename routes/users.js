const express = require('express');
const usersController = require('../controllers/users');
const router = express.Router();

router.get('/register', usersController.goToRegister);

router.get('/login', usersController.goToLogin);

router.post('/register', usersController.register);

router.post('/login', usersController.login);

router.get('/logout', usersController.logout);

module.exports = router;
