const express = require('express');
const { ensureAuth } = require('../config/auth');
const rootController = require('../controllers/root');
const router = express.Router();

router.get('/', rootController.goToHome);

router.get('/about', rootController.goToDocs);

router.get('/dashboard', ensureAuth, rootController.getAllCourses);

module.exports = router;
