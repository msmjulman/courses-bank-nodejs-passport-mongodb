const express = require('express');
const router = express.Router();

router.get('*', function (req, res) {
	res.status(404).render('404');
});

module.exports = router;
