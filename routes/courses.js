const express = require('express');
const { ensureAuth } = require('../config/auth');
const multer = require('../config/multer-config');
const coursesController = require('../controllers/courses');
const router = express.Router();

router.get('/ajouter', ensureAuth, coursesController.goToAddCourse);

router.post('/ajouter', ensureAuth, multer, coursesController.addCourse);

router.get('/view/:id', ensureAuth, coursesController.getOneCourse);

router.get('/editer/:id', ensureAuth, coursesController.goToUpdateCourse);

router.put('/editer/:id', ensureAuth, multer, coursesController.updateCourse);

router.delete('/delete/:id', ensureAuth, coursesController.deleteCourse);

router.post('/comment', ensureAuth, coursesController.commentCourse);

module.exports = router;
