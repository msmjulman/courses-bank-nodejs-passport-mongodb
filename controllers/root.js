const Course = require('../models/Course');

exports.goToHome = (req, res) => {
	res.render('home');
};

exports.goToDocs = (req, res) => {
	res.render('about');
};

exports.getAllCourses = (req, res) => {
	Course.find()
		.sort({ create_at: -1 })

		.then((courses) => {
			// console.log(courses);
			res.render('dashboard', {
				name: req.user.name,
				layout: 'layouts/admin',
				courses: courses,
			});
		})
		.catch((err) => {
			console.log(err);
		});
};
