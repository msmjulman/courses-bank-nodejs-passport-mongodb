const fs = require('fs');
const User = require('../models/User');
const Course = require('../models/Course');
const moment = require('moment');

exports.goToAddCourse = (req, res) => {
	res.render('add-course', { layout: 'layouts/admin' });
};

exports.addCourse = (req, res) => {
	const { title, description, category } = req.body;

	// console.log('MSM-Request', req.file);
	const errors = [];

	// Check empty fields
	if (!title || !description || !category) {
		errors.push({ message: 'Veuillez renseigner tous les champs !!!' });
	}

	// Check empty fields
	if (typeof req.file === 'undefined') {
		errors.push({ message: 'Veuillez choisir un document en format pdf !!!' });
	}

	// Error validation
	if (errors.length !== 0) {
		res.render('add-course', {
			layout: 'layouts/admin',
			errors,
			title,
			description,
			category,
		});
	} else {
		const newCourse = new Course({
			title,
			description,
			category,
			fileUrl: `/uploads/${req.file.filename}`,
			user: req.user._id,
		});

		if (!fs.existsSync('../uploads')) {
			fs.mkdir('../uploads', () => console.log('Folder is created...'));
		}

		newCourse
			.save()
			.then(() => {
				req.flash('success_msg', 'Document ajouté !!!');
				res.redirect('/dashboard');
			})
			.catch((err) => {
				console.log(err);
				req.flash('error_msg', 'Erreur server !!!');
				res.status(500).render('/dashboard');
			});
	}
};

exports.getOneCourse = async (req, res) => {
	const user = await User.findOne({ _id: req.user.id });

	Course.findOne({ _id: req.params.id })
		.then((course) => {
			// console.log(course);
			res.render('view-course', {
				layout: 'layouts/admin',
				course,
				username: user.name,
			});
		})
		.catch((err) => {
			res.status(404).render('404');
			console.log(err);
		});
};

exports.goToUpdateCourse = (req, res) => {
	Course.findOne({ _id: req.params.id })
		.then((course) => {
			res.render('edit-course', { layout: 'layouts/admin', course });
		})
		.catch((err) => {
			res.status(404).render('404');
			console.log(err);
		});
};

exports.updateCourse = (req, res) => {
	//
	const courseObj =
		typeof req.file !== 'undefined'
			? {
					title: req.body.title,
					description: req.body.description,
					category: req.body.category,
					fileUrl: `/uploads/${req.file.filename}`,
					user: req.user._id,
			  }
			: {
					title: req.body.title,
					description: req.body.description,
					category: req.body.category,
					user: req.user._id,
			  };
	const errors = [];

	// Check empty fields
	if (!courseObj.title || !courseObj.description || !courseObj.category) {
		errors.push({ message: 'Veuillez renseigner tous les champs !!!' });
	}

	// Error validation
	if (errors.length !== 0) {
		req.flash(
			'error_msg',
			"Echec modification document !!! Tous les champs n'ont pas été renseigné"
		);
		res.redirect('/dashboard');
	} else {
		if (typeof req.file !== 'undefined') {
			Course.findOne({ _id: req.params.id })
				.then((course) => {
					const filename = course.fileUrl.split('/uploads/')[1];
					fs.unlink(`uploads/${filename}`, () => {
						Course.updateOne({ _id: req.params.id })
							.then(() => {
								req.flash('success_msg', 'Document modifié !!!');
								res.redirect('/dashboard');
							})
							.catch((err) => {
								res.status(404).render('404');
								console.log(err);
							});
					});
				})
				.catch((err) => {
					res.status(404).render('404');
					console.log(err);
				});
		} else {
			Course.updateOne({ _id: req.params.id }, courseObj)
				.then(() => {
					req.flash('success_msg', 'Document modifié !!!');
					res.redirect('/dashboard');
				})
				.catch((err) => {
					res.status(404).render('404');
					console.log(err);
				});
		}
	}
};

exports.deleteCourse = (req, res) => {
	Course.findOne({ _id: req.params.id })
		.then((course) => {
			const filename = course.fileUrl.split('/uploads/')[1];
			fs.unlink(`uploads/${filename}`, () => {
				Course.deleteOne({ _id: req.params.id })
					.then(() => {
						req.flash('success_msg', 'Document supprimé !!!');
						res.redirect('/dashboard');
					})
					.catch((err) => console.log(err));
			});
		})
		.catch((err) => console.log(err));
};

exports.commentCourse = (req, res) => {
	const commentObj = {
		comment: req.body.comment,
		course_id: req.body.course,
		user_id: req.user._id,
		created_at: moment().format('L'),
	};

	Course.findOne({ _id: req.body.course })
		.then((course) => {
			console.log(course);
			course
				.update({ $push: { comments: commentObj } })
				.then(() => {
					req.flash('success_msg', 'Votre avis a été pris en compte');
					res.redirect(`/api/courses/view/${req.body.course}`);
				})
				.catch((err) => console.log(err));
		})
		.catch((err) => console.log(err));
};
