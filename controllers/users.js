const passport = require('passport');
const bcrypt = require('bcryptjs');
const User = require('../models/User');

exports.goToRegister = (req, res) => {
	res.render('register');
};

exports.register = (req, res) => {
	// res.render('register');
	const { name, email, password, passwordConf } = req.body;

	// Errors array
	const errors = [];

	// Check empty fields
	if (!name || !email || !password || !passwordConf) {
		errors.push({ message: 'Veuillez renseigner tous les champs !!!' });
	}

	// Check password length
	if (password && password.length < 6) {
		errors.push({ message: 'Mot de passe trop court!' });
	}

	// Check password length
	if (password && passwordConf && password !== passwordConf) {
		errors.push({ message: 'Vos deux mot de passe sont differents !!!' });
	}

	// Error validation
	if (errors.length !== 0) {
		res.render('register', {
			errors,
			name,
			email,
			password,
			passwordConf,
		});
	} else {
		User.findOne({ email: email })
			.then((user) => {
				if (user) {
					errors.push({
						message: 'Choississez un autre email, celui ci est déjà pris !!!',
					});
					res.render('register', {
						errors,
						name,
						email,
						password,
						passwordConf,
					});
				} else {
					const newUser = new User({
						name,
						email,
						password,
					});

					bcrypt.genSalt(10, (err, salt) => {
						bcrypt.hash(newUser.password, salt, (err, hash) => {
							newUser.password = hash;

							newUser
								.save()
								.then(() => {
									req.flash(
										'success_msg',
										'Inscription réussi!!! Vous pouvez vous connecter.'
									);
									res.redirect('/users/login');
								})
								.catch((err) => {
									console.log(err);
								});
						});
					});
				}
			})
			.catch((err) => {
				console.log(err);
			});
	}
};

exports.goToLogin = (req, res) => {
	res.render('login');
};

exports.login = (req, res, next) => {
	passport.authenticate('local', {
		successRedirect: '/dashboard',
		failureRedirect: '/users/login',
		failureFlash: true,
	})(req, res, next);
};

exports.logout = (req, res) => {
	req.logout();
	req.flash('success_msg', 'Vous êtes déconnecté');
	res.redirect('/users/login');
};
